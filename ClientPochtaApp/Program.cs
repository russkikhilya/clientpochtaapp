using System;

namespace ClientPochtaApp
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Hello! This is messages publisher. Enter your message.");

      while (true)
      {
        var message = Console.ReadLine();
        if (string.IsNullOrEmpty(message))
        {
          Console.WriteLine("You entered empty string");
        }
        else
        {
          var service = new RabbitPublishService();
          service.Publish(message);
          Console.WriteLine("Enter new message");
        }
      }
    }
  }
}
