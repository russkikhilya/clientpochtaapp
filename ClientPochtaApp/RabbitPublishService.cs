using ClientPochtaApp.Model;
using RabbitMQ.Client;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using System.Text.Json;

namespace ClientPochtaApp
{
  public class RabbitPublishService : IRabbitPublishService
  {
    private readonly ConnectionFactory _factory;
    private readonly ConcurrentDictionary<string, bool> _declaredExchanges;
    private readonly ConcurrentDictionary<string, bool> _declaredQueue;
    private readonly string queueName;
    private readonly string exchangeName;
    private readonly string routingKey;

    public RabbitPublishService()
    {
      _factory = new ConnectionFactory()
      {
        UserName = Configuration.RabbitConnectionOptions.UserName,
        Password = Configuration.RabbitConnectionOptions.Password,
        HostName = Configuration.RabbitConnectionOptions.HostName,
        Port = AmqpTcpEndpoint.UseDefaultPort,
        VirtualHost = "/",
      };

      _declaredExchanges = new ConcurrentDictionary<string, bool>();
      _declaredQueue = new ConcurrentDictionary<string, bool>();

      exchangeName = Configuration.RabbitParams.ExchangeName;
      queueName = Configuration.RabbitParams.QueueName;
      routingKey = Configuration.RabbitParams.RoutingKey;
    }

    private IModel CreateChannel()
    {
      var connection = _factory.CreateConnection();
      return connection.CreateModel();
    }

    /// <summary>
    /// Опубликовать сообщени в rabbit
    /// </summary>
    /// <param name="message"></param>
    public void Publish(string message)
    {
      try 
      {
        using (var channel = CreateChannel())
        {
          DeclareExchange(exchangeName, channel);
          DeclareQueue(queueName, channel);
          channel.QueueBind(queueName, exchangeName, routingKey);
          channel.BasicPublish(exchangeName, routingKey, false, null, CreatePublishingMessage(message));
          Console.WriteLine("Message publish success.");
        }
      }
      catch
      {
        Console.WriteLine("Publish message was failed.");
      }
    }

    /// <summary>
    /// Объявление обменника для rabbit
    /// </summary>
    /// <param name="exchangeName"></param>
    /// <param name="channel"></param>
    private void DeclareExchange(string exchangeName, IModel channel)
    {
      _declaredExchanges.GetOrAdd(exchangeName, exchange =>
      {
        channel.ExchangeDeclare(exchangeName, ExchangeType.Topic, durable: true);
        return true;
      });
    }

    /// <summary>
    /// Объявление очереди для rabbit
    /// </summary>
    /// <param name="queueName"></param>
    /// <param name="channel"></param>
    private void DeclareQueue(string queueName, IModel channel)
    {
      _declaredQueue.GetOrAdd(queueName, exchange =>
      {
        channel.QueueDeclare(queueName, durable: true, autoDelete: false, exclusive: false);
        return true;
      });
    }


    /// <summary>
    /// Создать сообщения для публикации в rabbit
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    private byte[] CreatePublishingMessage(string message)
    {
      var hostName = Dns.GetHostName();
      var ipAddress = Dns.GetHostAddresses(hostName).FirstOrDefault(a => IPAddress.Parse(a.ToString()).AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
      return JsonSerializer.SerializeToUtf8Bytes(new PublishingMessage() { Message = message, IPClient = ipAddress.ToString() });
    }
  }
}
