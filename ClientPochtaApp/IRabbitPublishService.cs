namespace ClientPochtaApp
{
  interface IRabbitPublishService
  {
    void Publish(string message);
  }
}
