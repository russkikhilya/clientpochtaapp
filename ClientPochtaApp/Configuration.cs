using ClientPochtaApp.Model;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ClientPochtaApp
{
  public static class Configuration
  {
    private static IConfigurationRoot _configurationRoot;

    public static RabbitConnectionOptions RabbitConnectionOptions { get; }

    public static RabbitParams RabbitParams { get; }

    static Configuration()
    {
      _configurationRoot = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        .Build();
      var connectionSection = GetSectionConf(nameof(RabbitConnectionOptions));
      RabbitConnectionOptions = new RabbitConnectionOptions()
      {
        HostName = connectionSection["Hostname"],
        Password = connectionSection["Password"],
        UserName = connectionSection["Username"]
      };
      var rabbitParamsSection = GetSectionConf(nameof(RabbitParams));
      RabbitParams = new RabbitParams()
      {
        ExchangeName = rabbitParamsSection["Exchange"],
        QueueName = rabbitParamsSection["Queue"],
        RoutingKey = rabbitParamsSection["Routingkey"]
      };
    }

    /// <summary>
    /// Получить блок конфигурации по имени
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private static IConfigurationSection GetSectionConf(string name)
    {
      return _configurationRoot.GetSection(name);
    }
  }
}
