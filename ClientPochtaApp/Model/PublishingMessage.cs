namespace ClientPochtaApp.Model
{
  /// <summary>
  /// Публикуемое сообщение
  /// </summary>
  public class PublishingMessage
  {
    /// <summary>
    /// Тело сообщения
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// Ip клиента
    /// </summary>
    public string IPClient { get; set; }
  }
}
