namespace ClientPochtaApp.Model
{
  /// <summary>
  /// Настройки подлючения к rabbit
  /// </summary>
  public class RabbitConnectionOptions
  {
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// Пароль пользователя
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    /// Имя хоста
    /// </summary>
    public string HostName { get; set; }
  }
}
